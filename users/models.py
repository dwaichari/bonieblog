import os

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.dispatch import receiver
from django.urls import reverse


class CustomUser(AbstractUser):
    # Add additional fields here. For example, a global-friendly name field
    avatar = models.ImageField(upload_to="avatars", default='avatar.png')
    def __str__(self):
        if  self.first_name and self.last_name:
            return self.last_name + ' '+ self.first_name
        else:
            return self.username

    def get_absolute_url(self):
        return reverse('home')

@receiver(models.signals.post_delete, sender=CustomUser)
def auto_delete_file_on_delete(sender, instance, **kwargs):
        """
        Deletes file from filesystem
        when corresponding `MediaFile` object is deleted.
        """
        if instance.avatar:
            if os.path.isfile(instance.avatar.path):
                if not instance.avatar == 'avatar.png':
                    os.remove(instance.avatar.path)

@receiver(models.signals.pre_save, sender=CustomUser)
def auto_delete_file_on_change(sender, instance, **kwargs):
        """
        Deletes old file from filesystem
        when corresponding `MediaFile` object is updated
        with new file.
        """
        if not instance.pk:
            return False

        try:
            old_file = sender.objects.get(pk=instance.pk).avatar
        except sender.DoesNotExist:
            return False

        new_file = instance.avatar

        if not old_file == new_file:
            if os.path.isfile(old_file.path):
                if not old_file == 'avatar.png':
                    os.remove(old_file.path)