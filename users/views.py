from django.shortcuts import render

# Create your views here.
from django.views.generic import UpdateView

from myblog.models import Category, Post, Tag
from users.forms import ProfileUpdateForm
from users.models import CustomUser


class ProfileUpdateView(UpdateView):
    model = CustomUser
    form_class = ProfileUpdateForm
    def get_context_data(self, **kwargs):
        context = super(ProfileUpdateView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['posts'] = Post.objects.all()
        context['tags'] = Tag.objects.all()
        return context
