from django.urls import path, include
from django.contrib.auth.decorators import login_required
from . import views
urlpatterns = [
    path('', views.index, name='home'),

    #urls for category
    path('myblog/category/', views.CategoryListView.as_view(), name='myblog_category_list'),
    path('myblog/category/create/', login_required(views.CategoryCreateView.as_view()), name='myblog_category_create'),
    path('myblog/category/detail/<slug:slug>/', views.CategoryDetailView.as_view(), name='myblog_category_detail'),
    path('myblog/category/update/<slug:slug>/', login_required(views.CategoryUpdateView.as_view()), name='myblog_category_update'),

    #urls for tag
    path('myblog/tag/', views.TagListView.as_view(), name='myblog_tag_list'),
    path('myblog/tag/create/', login_required(views.TagCreateView.as_view()), name='myblog_tag_create'),
    path('myblog/tag/detail/<slug:slug>/', views.TagDetailView.as_view(), name='myblog_tag_detail'),
    path('myblog/tag/update/<slug:slug>/', login_required(views.TagUpdateView.as_view()), name='myblog_tag_update'),

    # urls for Post
    path('myblog/post/', views.PostListView.as_view(), name='myblog_post_list'),
    path('myblog/post/create/', login_required(views.PostCreateView.as_view()),
         name='myblog_post_create'),
    path('myblog/post/detail/<slug:slug>/',
         views.PostDetailView.as_view(), name='myblog_post_detail'),
    path('myblog/post/update/<slug:slug>/',
         login_required(views.PostUpdateView.as_view()), name='myblog_post_update'),
]
