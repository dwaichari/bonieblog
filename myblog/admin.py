from django.contrib import admin
from django import forms
from .models import Post

class PostAdminForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = '__all__'


class PostAdmin(admin.ModelAdmin):
    form = PostAdminForm
    list_display = ['title', 'slug', 'created', 'last_updated', 'image', 'body']
    #readonly_fields = ['title', 'slug', 'created', 'last_updated', 'image', 'body']

admin.site.register(Post, PostAdmin)
