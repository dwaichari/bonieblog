from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from searchableselect.widgets import SearchableSelect

from .models import Post, Category, Tag


class PostForm(forms.ModelForm):
    tags = SearchableSelect(model='myblog.Tag', search_field='name', many=True)
    class Meta:
        model = Post
        fields = ['category','title', 'image', 'body','summary','tags', 'featured']

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['name']

class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ['name']