from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import DetailView, ListView, UpdateView, CreateView

from users.models import CustomUser
from .models import Post, Category, Tag
from .forms import PostForm, CategoryForm, TagForm


# Create your views here.
def index(request):
    context = {
        'categories': Category.objects.all(),
        'posts': Post.objects.all(),
        'tags': Tag.objects.all(),
    }
    return  render(request, '_base.html', context)

#views for category model
class CategoryListView(ListView):
    model = Category
    def get_context_data(self, **kwargs):
        context = super(CategoryListView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['posts'] = Post.objects.all()
        context['tags'] = Tag.objects.all()
        return context


class CategoryCreateView(CreateView):
    model = Category
    form_class = CategoryForm
    def get_context_data(self, **kwargs):
        context = super(CategoryCreateView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['posts'] = Post.objects.all()
        context['tags'] = Tag.objects.all()
        return context



class CategoryDetailView(DetailView):
    model = Category
    def get_context_data(self, **kwargs):
        context = super(CategoryDetailView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['posts'] = Post.objects.all()
        context['tags'] = Tag.objects.all()
        return context


class CategoryUpdateView(UpdateView):
    model = Category
    form_class = CategoryForm
    def get_context_data(self, **kwargs):
        context = super(CategoryUpdateView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['posts'] = Post.objects.all()
        context['tags'] = Tag.objects.all()
        return context

#views for tag model
class TagListView(ListView):
    model = Tag
    def get_context_data(self, **kwargs):
        context = super(TagListView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['posts'] = Post.objects.all()
        context['tags'] = Tag.objects.all()
        return context

class TagCreateView(CreateView):
    model = Tag
    form_class = TagForm
    def get_context_data(self, **kwargs):
        context = super(TagCreateView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['posts'] = Post.objects.all()
        context['tags'] = Tag.objects.all()
        return context

class TagDetailView(DetailView):
    model = Tag
    def get_context_data(self, **kwargs):
        context = super(TagDetailView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['posts'] = Post.objects.all()
        context['tags'] = Tag.objects.all()
        return context

class TagUpdateView(UpdateView):
    model = Tag
    form_class = TagForm
    def get_context_data(self, **kwargs):
        context = super(TagUpdateView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['posts'] = Post.objects.all()
        context['tags'] = Tag.objects.all()
        return context

#views for post
class PostListView(ListView):
    model = Post
    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['posts'] = Post.objects.all()
        context['tags'] = Tag.objects.all()
        return context

class PostCreateView(CreateView):
    model = Post
    form_class = PostForm
    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.added_by = CustomUser.objects.get(id=self.request.user.id)
        instance.save()
        form.save_m2m()
        return redirect('myblog_post_detail', instance.slug)

    def get_context_data(self, **kwargs):
        context = super(PostCreateView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['posts'] = Post.objects.all()
        context['tags'] = Tag.objects.all()
        return context

class PostDetailView(DetailView):
    model = Post

    def get_context_data(self, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['posts'] = Post.objects.all()
        post = Post.objects.get(slug = self.kwargs['slug'] )
        context['post_tags'] = post.tags.all()
        context['tags'] = Tag.objects.all()
        return context

class PostUpdateView(UpdateView):
    model = Post
    form_class = PostForm

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.added_by = CustomUser.objects.get(id=self.request.user.id)
        instance.save()
        form.save_m2m()
        return redirect('myblog_post_detail', instance.slug)

    def get_context_data(self, **kwargs):
        context = super(PostUpdateView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        post = Post.objects.get(slug=self.kwargs['slug'])
        context['post_tags'] = post.tags.all()
        context['posts'] = Post.objects.all()
        context['tags'] = Tag.objects.all()
        return context