import os

from django.dispatch import receiver
from django.urls import reverse
from django.db import models as models
from django_extensions.db import fields as extension_fields
from django_extensions.db.fields import AutoSlugField
from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import ImageField
from django.db.models import TextField
from django_extensions.db.fields import AutoSlugField
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models

class Category(models.Model):
    name = models.CharField(max_length=255)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('myblog_category_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('myblog_category_update', args=(self.slug,))

    def __str__(self):
        return self.name

class Tag(models.Model):
    name = models.CharField(max_length=255)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('myblog_tag_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('myblog_tag_update', args=(self.slug,))

    def __str__(self):
        return self.name


class Post(models.Model):
    # Fields
    title = models.CharField(max_length=255)
    slug = extension_fields.AutoSlugField(populate_from='title', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    image = models.ImageField(upload_to="post-images", default='default.png')
    body = models.TextField()
    summary = models.TextField()
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True)
    tags = models.ManyToManyField(Tag)
    featured = models.BooleanField(default=False)


    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('myblog_post_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('myblog_post_update', args=(self.slug,))

    def __str__(self):
        return self.title

@receiver(models.signals.post_delete, sender=Post)
def auto_delete_file_on_delete(sender, instance, **kwargs):
        """
        Deletes file from filesystem
        when corresponding `MediaFile` object is deleted.
        """
        if instance.image:
            if os.path.isfile(instance.image.path):
                if not instance.image == 'default.png':
                    os.remove(instance.image.path)

@receiver(models.signals.pre_save, sender=Post)
def auto_delete_file_on_change(sender, instance, **kwargs):
        """
        Deletes old file from filesystem
        when corresponding `MediaFile` object is updated
        with new file.
        """
        if not instance.pk:
            return False

        try:
            old_file = sender.objects.get(pk=instance.pk).image
        except sender.DoesNotExist:
            return False

        new_file = instance.image
        if not old_file == new_file:
            if os.path.isfile(old_file.path):
                if not old_file == 'default.png':
                    os.remove(old_file.path)
